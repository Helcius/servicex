package br.com.diegopatricio.servicex.servico.resources;

import br.com.diegopatricio.servicex.categoria.domain.Categoria;
import br.com.diegopatricio.servicex.categoria.services.CategoriaService;
import br.com.diegopatricio.servicex.servico.domain.Servico;
import br.com.diegopatricio.servicex.servico.domain.ServicoDTO;
import br.com.diegopatricio.servicex.servico.services.ServicoService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping(value = "/servicos")
@Tag(name = "Manter Serviço", description = "Funcionalidade das operações de Serviços")
public class ServicoResource {

    @Autowired
    private ServicoService servicoService;

    @Operation(summary = "Cadastrar Serviço", description = "O recurso permite cadastrar um novo serviço, porém não pode repetir o mesmo nome.")
    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping
    public ResponseEntity<Servico> criarServico(@RequestBody ServicoDTO servicoDTO) {
        Servico servico = servicoService.fromDTOService(servicoDTO);
        servico = servicoService.criarServico(servico);
        URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{id}")
                .buildAndExpand(servico.getIdServico()).toUri();
        return  ResponseEntity.created(uri).build();
    }

    @Operation(summary = "Listar Servicos", description = "O recurso lista todas os serviços.")
    @PreAuthorize("hasAnyRole('ADMIN', 'USER')")
    @GetMapping
    public ResponseEntity<List<Servico>> listarServicos() {
        List<Servico> servicos = servicoService.listarServico();
        return new ResponseEntity<>(servicos, HttpStatus.OK);
    }
    @Operation(summary = "Listar Serviço por Id", description = "O recurso busca um Serviço por Id.")
    @PreAuthorize("hasAnyRole('ADMIN', 'USER')")
    @GetMapping("/{idServico}")
    public ResponseEntity<Servico> buscarServico(@PathVariable Integer idServico) {
        Servico servico = servicoService.buscarServico(idServico);
        return ResponseEntity.ok().body(servico);
    }

    @Operation(summary = "Atualizar Serviço por Id", description = "O recurso modifica um Serviço por Id.")
    @PreAuthorize("hasRole('ADMIN')")
    @PutMapping("/{idServico}")
    public ResponseEntity<Servico> atualizarServico(@PathVariable Integer idServico, @RequestBody ServicoDTO servicoDTO) {
        Servico servico = servicoService.fromDTOService(servicoDTO);
        servico.setIdServico(idServico);
        servicoService.atualizarServico(servico);
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "Deletar Serviço por Id", description = "O recurso deleta um Serviço por Id.")
    @PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping("/{idServico}")
    public ResponseEntity<Void> deletarServico(@PathVariable Integer idServico) {
        servicoService.deletarServico(idServico);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }


}
