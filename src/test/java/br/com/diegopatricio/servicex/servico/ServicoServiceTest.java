package br.com.diegopatricio.servicex.servico;

import br.com.diegopatricio.servicex.categoria.domain.Categoria;
import br.com.diegopatricio.servicex.categoria.repositories.CategoriaRepository;
import br.com.diegopatricio.servicex.servico.domain.Servico;
import br.com.diegopatricio.servicex.servico.repositories.ServicoRepository;
import br.com.diegopatricio.servicex.servico.services.ServicoService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.DataIntegrityViolationException;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class ServicoServiceTest {

    @InjectMocks
    private ServicoService servicoService;

    @Mock
    private ServicoRepository servicoRepository;

    @Mock
    private CategoriaRepository categoriaRepository;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testCriarServico_ComSucesso() {
        Servico servico = new Servico();
        servico.setNome("Novo Serviço");

        Servico servicoSalvo = new Servico();
        servicoSalvo.setIdServico(1);
        servicoSalvo.setNome("Novo Serviço");

        Categoria categoria = new Categoria();
        categoria.setIdCategoria(1);
        categoria.setNomeCategoria("Categoria Teste");

        when(servicoRepository.save(any(Servico.class))).thenReturn(servicoSalvo);
        when(categoriaRepository.findByServicos_IdServico(servicoSalvo.getIdServico())).thenReturn(categoria);

        Servico result = servicoService.criarServico(servico);

        Assertions.assertEquals(servicoSalvo.getIdServico(), result.getIdServico());
        Assertions.assertEquals(categoria, result.getCategoria());
        verify(servicoRepository, times(1)).save(any(Servico.class));
        verify(categoriaRepository, times(1)).findByServicos_IdServico(servicoSalvo.getIdServico());
    }

    @Test
    public void testCriarServico_CategoriaInexistente() {
        Servico servico = new Servico();
        servico.setNome("Novo Serviço");
        when(servicoRepository.save(any(Servico.class))).thenThrow(new DataIntegrityViolationException("Categoria informada inexistete!"));
        DataIntegrityViolationException exception = Assertions.assertThrows(DataIntegrityViolationException.class, () -> servicoService.criarServico(servico));

        Assertions.assertEquals("Categoria informada inexistete!", exception.getMessage());
        verify(servicoRepository, times(1)).save(any(Servico.class));
        verify(categoriaRepository, times(0)).findByServicos_IdServico(any());
    }
}
