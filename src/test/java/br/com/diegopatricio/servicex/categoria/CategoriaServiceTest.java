package br.com.diegopatricio.servicex.categoria;

import br.com.diegopatricio.servicex.categoria.domain.Categoria;
import br.com.diegopatricio.servicex.categoria.repositories.CategoriaRepository;
import br.com.diegopatricio.servicex.categoria.services.CategoriaService;
import br.com.diegopatricio.servicex.exceptions.ObjectNotFoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Optional;

import static org.mockito.Mockito.when;

public class CategoriaServiceTest {
    @InjectMocks
    private CategoriaService categoriaService;

    @Mock
    private CategoriaRepository categoriaRepository;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testBuscarCategoriaPorId_ComSucesso() {
        Integer id = 1;
        Categoria categoria = new Categoria(id, "Categoria Teste");
        when(categoriaRepository.findById(id)).thenReturn(Optional.of(categoria));
        Categoria result = categoriaService.buscarCategoriaPorId(id);

        Assertions.assertEquals(categoria, result);
    }

    @Test
    public void testBuscarCategoriaPorId_ObjetoNaoEncontrado() {
        Integer id = 1;
        String expectedMessage = "Objeto não encontrato! ID: " + id + ", Tipo: " + Categoria.class.getName();

        when(categoriaRepository.findById(id)).thenReturn(Optional.empty());
        ObjectNotFoundException exception = Assertions.assertThrows(ObjectNotFoundException.class, () -> categoriaService.buscarCategoriaPorId(id));
        System.out.println(exception.getMessage());
        Assertions.assertEquals(expectedMessage, exception.getMessage());
    }

}
